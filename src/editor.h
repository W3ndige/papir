#ifndef __EDITOR_H__
#define __EDITOR_H__

#include "includes.h"

struct refresh_buffer {
  char *container;
  size_t len;
};

#define CTRL_KEY(k) ((k) & 0x1f)
#define REFRESH_BUFFER_INIT {NULL, 0}
#define VERSION "0.0.0.1"

void refreshScreen();
char readKey();
void processKeypress();

#endif
