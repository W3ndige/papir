#ifndef __INCLUDES_H__
#define __INCLUDES_H__

#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <string.h>

#include "editor.h"
#include "terminal.h"
#include "files.h"

struct editor_row {
  int length;
  char *characters;
};

struct editor_config {
  size_t cursor_x;
  size_t cursor_y;
  size_t row_offset;
  size_t screenrows;
  size_t screencols;
  size_t num_of_rows;
  struct editor_row *row;
  struct termios original_termios;
} config;

#endif
