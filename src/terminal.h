#ifndef __TERMINAL_H__
#define __TERMINAL_H__

#include "includes.h"

void die(const char *error);
void disableRawMode();
void enableRawMode();
int getWindowSize(size_t *cols, size_t *rows);

#endif
