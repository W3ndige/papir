#include "editor.h"

void appendToRefreshBuffer(struct refresh_buffer *buffer,
                          const char *string, size_t len) {
  char *new = realloc(buffer->container, buffer->len + len);
  if (new == NULL) {
    return;
  }
  memcpy(&new[buffer->len], string, len);
  buffer->container = new;
  buffer->len += len;
}

void freeRefreshBuffer(struct refresh_buffer *buffer) {
  free(buffer->container);
}

void scrollEditor() {
  if (config.cursor_y < config.row_offset) {
    config.row_offset = config.cursor_y;
  }
  if (config.cursor_y >= config.row_offset + config.screenrows) {
    config.row_offset = config.cursor_y - config.screenrows + 1;
  }
}

void drawRows(struct refresh_buffer *buffer) {
  for (size_t i = 0; i < config.screenrows; i++) {
    size_t file_row = i + config.row_offset;
    if (file_row >= config.num_of_rows) {
      if (config.num_of_rows == 0 && i == config.screenrows / 3) {
        char welcome_message[80];
        size_t welcome_len = snprintf(welcome_message, sizeof(welcome_message),
                                        "Papir editor -- version 1%s", VERSION);

        if (welcome_len > config.screencols) {
          welcome_len = config.screencols;
        }
        size_t padding = (config.screencols - welcome_len) / 2;
        if (padding) {
          appendToRefreshBuffer(buffer, ">", 1);
          padding--;
        }
        while (padding--) {
          appendToRefreshBuffer(buffer, " ", 1);
        }
        appendToRefreshBuffer(buffer, welcome_message, welcome_len);
      }
      else {
        appendToRefreshBuffer(buffer, ">", 1);
      }
    }
    else {
      size_t len = config.row[file_row].length;
      if (len > config.screencols) {
        len = config.screencols;
      }
      appendToRefreshBuffer(buffer, config.row[file_row].characters, len);
    }
    appendToRefreshBuffer(buffer, "\x1b[K", 3);
    if (i < config.screenrows - 1) {
      appendToRefreshBuffer(buffer, "\r\n", 2);
    }
  }
}

void refreshScreen() {
  scrollEditor();

  struct refresh_buffer buffer = REFRESH_BUFFER_INIT;
  appendToRefreshBuffer(&buffer, "\x1b[?25l", 6);
  appendToRefreshBuffer(&buffer, "\x1b[H", 3);
  drawRows(&buffer);

  char cursor_buffer[32];
  snprintf(cursor_buffer, sizeof(cursor_buffer), "\x1b[%ld;%ldH", (config.cursor_y - config.row_offset) + 1, config.cursor_x + 1);
  appendToRefreshBuffer(&buffer, cursor_buffer, strlen(cursor_buffer));

  appendToRefreshBuffer(&buffer, "\x1b[?25h", 6);
  write(STDOUT_FILENO, buffer.container, buffer.len);
  freeRefreshBuffer(&buffer);
}

char readKey() {
  int nread;
  char character;
  while ((nread = read(STDIN_FILENO, &character, 1)) != 1) {
    if (nread == -1) {
      die("read");
    }
  }

  return character;
}

void moveCursor(char key) {
  switch (key) {
    case 'i':
      if (config.cursor_y != 0) {
        config.cursor_y--;
      }
      break;
    case 'k':
      if (config.cursor_y < config.num_of_rows) {
        config.cursor_y++;
      }
      break;
    case 'j':
      if (config.cursor_x != 0) {
        config.cursor_x--;
      }
      break;
    case 'l':
      if (config.cursor_x != config.screencols) {
          config.cursor_x++;
      }
      break;
  }
}

void processKeypress() {
  int write_mode = 0;
  char character = readKey();
  switch (character) {
    case CTRL_KEY('q'):
      write(STDOUT_FILENO, "\x1b[2J", 4);
      write(STDOUT_FILENO, "\x1b[H", 3);
      exit(0);
      break;
    case CTRL_KEY('i'):
      write_mode = ~write_mode;
      break;
    case 'i':
      moveCursor(character);
      break;
    case 'k':
      moveCursor(character);
      break;
    case 'j':
      moveCursor(character);
      break;
    case 'l':
      moveCursor(character);
      break;
  }
}
