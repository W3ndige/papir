#include "files.h"

void appendToRow(char *content, size_t len) {
  config.row = realloc(config.row, sizeof(struct editor_row) * (config.num_of_rows + 1));

  int at = config.num_of_rows;
  config.row[at].length = len;
  config.row[at].characters = malloc(len + 1);
  memcpy(config.row[at].characters, content, len);
  config.row[at].characters[len] = '\0';
  config.num_of_rows++;
}

void openEditor(char *filename) {
  FILE *file_to_read = fopen(filename, "r");
  if (file_to_read == NULL) {
    die("fopen");
  }

  char *line = NULL;
  size_t line_cap = 0;
  int line_len;
  while ((line_len = getline(&line, &line_cap, file_to_read)) != -1) {
    while (line_len > 0 && (line[line_len - 1] == '\n' ||
                           line[line_len - 1] == '\r')) {
      line_len--;
    }
    appendToRow(line, line_len);
  }
  free(line);
  fclose(file_to_read);
}
