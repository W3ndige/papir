#include "includes.h"

void init() {
  config.cursor_x = 0;
  config.cursor_y = 0;
  config.row_offset = 0;
  config.num_of_rows = 0;
  config.row = NULL;
  if (getWindowSize(&config.screenrows, &config.screencols) == -1) {
    die("getWindowSize");
  }
}

int main(int argc, char **argv) {
  enableRawMode();
  init();
  if (argc >= 2) {
    openEditor(argv[1]);
  }

  while (1) {
    refreshScreen();
    processKeypress();
  }
  return 0;

}
