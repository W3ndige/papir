OBJ = src/main.c src/terminal.c src/editor.c src/files.c
CC = gcc
COMPILER_FLAGS = -Wall -Wextra
OBJ_NAME = papir

all: $(OBJ)
	$(CC) $(OBJ) $(COMPILER_FLAGS) -o $(OBJ_NAME)

clean:
	rm $(OBJ_NAME)
